const mongoose = require('mongoose')
const mongoosastic = require('mongoosastic');

const roleSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique:true,
    },
    description: String,
    sub_roles:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Role',
    }],
    permissions:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Action',
    }],
});

roleSchema.methods.checkAccess =async function checkAccess(action_url,action_type){
    for (let index = 0; index < this.permissions.length; index++) {
        const permission =await Action.findById(this.permissions[index]);
        if(permission.checkAccess(action_url,action_type))
            {
                return true;
            }
    }

    for (let index = 0; index < this.sub_roles.length; index++) {
        const subRole=await Role.findById(this.sub_roles[index]);
        if(await subRole.checkAccess(action_url,action_type))
            {
                return true
            }
    }
    return false;
  }


const actionSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true,
    },
    type: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        required: true,
    },
})

actionSchema.methods.checkAccess =function checkAccess(url,type){
    if(this.type!=='*' && this.type!==type)
        return false;

    if(this.url.includes('*'))
    {
        var index = this.url.indexOf('*');
        return this.url.substring(0,index)===url.substring(0,index);
    }
    return this.url===url;
  }

roleSchema.plugin(mongoosastic, {
    host: 'localhost',
    port: 9200,
});
actionSchema.plugin(mongoosastic, {
    host: 'localhost',
    port: 9200,
});


const Role = mongoose.model('Role', roleSchema)
const Action = mongoose.model('Action', actionSchema)


exports.Role = Role;
exports.Action = Action;
