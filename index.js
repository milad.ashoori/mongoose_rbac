const { Role,Action } = require("./models");

class MongooseRbac {

    /**
     * Add new role.
     * @param {string} title Title
     * @param {string} description Description
     * 
     * @returns {string} New role id or null if occure an error.
     */
    AddNewRole=async (title,description)=>{
        var newRole=new Role({title,description});
        try {
            const res = await newRole.save();
            return res._id;
        }
        catch (err) {
            return null;
        }
    }

    /**
     * Remove a role
     * @param {string} _id Role Id
     */
    RemoveRole=async (_id)=>{
        const re = await Role.findByIdAndRemove({ _id });
        return re !== null;
    }

    /**
     * Get roles list.
     */
    RolesList=()=>{
        return Role.find()
        .populate('sub_roles')
        .populate('permissions')
        .lean()
        .exec();
    }

    /**
     * Get a role.
     * @param {string} _id Role's Id.
     */
    GetRole=async (_id)=>{
        try {
            const res = await Role.findById({ _id })
                .populate('sub_roles')
                .populate('permissions')
                .lean()
                .exec();
            return res;
        }
        catch (err) {
            return null;
        }
    }


    /**
     * Add new action.
     * @param {string} title Title
     * @param {string} type Type of action
     * @param {string} url Path of action
     */
    AddNewAction=async (title,type,url)=>{
        var newAction=new Action({title,type,url});
        try {
            const res = await newAction.save();
            return res._id;
        }
        catch (err) {
            return null;
        }
    }

    /**
     * Remove an action.
     * @param {string} _id Action's Id
     */
    RemoveAction=async (_id)=>{
        const re = await Action.findByIdAndRemove({ _id });
        return re !== null;
    }

    /**
     * Get actions list.
     */
    ActionsList=()=>{
        return Action.find()
        .lean()
        .exec();
    }

    /**
     * Get an action.
     * @param {string} _id Action's Id
     */
    GetAction=async (_id)=>{
        try {
            const res = await Action.findById({ _id })
                .lean()
                .exec();
            return res;
        }
        catch (err) {
            return null;
        }
    }

    /**
     * Add a role to parent's sub role list.
     * @param {string} parent_role_id Parent role Id
     * @param {string} sub_role_id Sub role Id.
     */
    AddSubRole=async (parent_role_id,sub_role_id)=>{
        const res = await Role.update(
            { _id: parent_role_id },
            { $push: { sub_roles: sub_role_id } });
        return res;
    }

    /**
     * Remove a role from parent's sub role list.
     * @param {string} parent_role_id Parent role Id
     * @param {string} sub_role_id Sub role Id
     */
    RemoveSubRole=async (parent_role_id,sub_role_id)=>{
        const res = await Role.update(
            { _id: parent_role_id },
            { $pull: { sub_roles: sub_role_id } },
            { safe: true, multi: true });
        return res;
    }

    /**
     * Add permission of an action to a role.
     * @param {string} role_id Role Id
     * @param {string} action_id Action Id
     */
    AddPermission=async (role_id,action_id)=>{
        const res = await Role.updateOne(
            { _id: role_id },
            { $push: { permissions: action_id } });
        return res;
    }

    /**
     * Remove an action permission from a role.
     * @param {string} role_id Role Id.
     * @param {string} action_id Action Id.
     */
    RemovePermission=async (role_id,action_id)=>{
        const res = await Role.updateOne(
            { _id: role_id },
            { $pull: { permissions: action_id } },
            { safe: true, multi: true });
        return res;
    }

    /**
     * Check permission of an action for a role by role title.
     * @param {string} role_title Title of user role.
     * @param {string} action_url Action which need check permission.
     * @param {string} action_type Type of action.
     */
    CheckAccess=async (role_title,action_url,action_type)=>{
        const res = await Role.findOne({ title: role_title });
        return res.checkAccess(action_url, action_type);
    }

}
module.exports = MongooseRbac
