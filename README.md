<p align="center">
  <img src="https://iot.miluxas.ir/img/logoC.png" alt="Miluxas Logo">
</p>

# Mongoose Base RBAC 

Mongoose_rbac is Role Base Access Control that based on mongoose data base.

Mongoose_rbac does **not** require *any* additional changes to your method of development.

[![NPM version](https://badge.fury.io/js/nodemon.svg)](https://www.npmjs.com/package/@miluxas/mongoose_rbac)

# Installation

By using [npm](http://npmjs.org) (the recommended way):

```bash
npm i @miluxas/mongoose_rbac
```


# Usage

Create an object of model:

```js
const mongooseRbac = new (require('mongoose_rbac'))();
```



Create some **role** with: 

```js
    mongooseRbac.AddNewRole('Role Title','Description of role')
```


Create some **action** with: 

```js
    mongooseRbac.AddNewAction('Action Title','Action type','Action url')
```
If enter * for type of action then all type are accepted.
If enter a * in end of url then only before of * is checked.

Add sub role to a parent role with: 

```js
    mongooseRbac.AddSubRole('Parent role Id','Sub role Id')
```
A role has all sub role permissions.


Add permission of an action to a role with: 

```js
    mongooseRbac.AddPermission('Role Id','Action Id')
```


And finally before each action we can check the access of user's role to it.

```js
    mongooseRbac.CheckAccess('Title of role','action url','Type of action')
        .then(hasAccess=>{
            // if role of user has access to this action return true, else return false
            console.log(hasAccess)
        }); 
```

## Sponsors

Support this project by becoming a sponsor. Your logo will show up here with a link to your website. [Sponsor this project today ❤️](https://iot.miluxas.ir)


# License

MIT [http://rem.mit-license.org](http://rem.mit-license.org)

